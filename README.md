This is a starter template for [Ionic](http://ionicframework.com/docs/) projects.

## How to use this template

*This template does not work on its own*. The shared files for each starter are found in the [ionic2-app-base repo](https://github.com/ionic-team/ionic2-app-base).

To use this template, either create a new ionic project using the ionic node.js utility, or copy the files from this repository into the [Starter App Base](https://github.com/ionic-team/ionic2-app-base).

### With the Ionic CLI:

Take the name after `ionic2-starter-`, and that is the name of the template to be used when using the `ionic start` command below:

```bash
$ sudo npm install -g ionic cordova
$ ionic start myBlank blank
```

Then, to run it, cd into `myBlank` and run:

```bash
$ ionic cordova platform add ios
$ ionic cordova run ios
```

Substitute ios for android if not on a Mac.

--------COSAS POR HACER----------

Explicar los pasos para instalar paquetes y plugins

Para BADGES:
```bash
$ ionic cordova plugin add cordova-plugin-badge
$ npm install --save @ionic-native/badge
```
Para LOCAL NOTIFICATION:

```bash
cordova plugin add https://github.com/DavidBriglio/cordova-plugin-local-notifications
```

    - En [home.ts] asegurarse del nombre del archivo para el [icon]. Despues se entra a la direccion "\platforms\android\res\drawable" y modificar los iconos por los que se desee que se muestren.

    - En [home.ts] asegurarse del nombre del archivo para el sonido para el [sound]. Despues colocar los sonidos en la carpeta "\www\audio" y ADEMAS, en ANDROID, modificar el archivo AndroidManifest.xml y modificar asegurarse que el targetSDK sea 23.[android:targetSdkVersion="23"]
