import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Badge } from '@ionic-native/badge';

declare var cordova;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	public id = 0;
	public desc = "";

	constructor(public navCtrl: NavController, private plt: Platform, private localNotifications: LocalNotifications, public alertCtrl: AlertController, private badge: Badge) {
		this.plt.ready().then((readySource) => {
	  		this.localNotifications.on('trigger', () => {
	  		this.badge.increase(1);
	  	})

	    this.localNotifications.on('click', (notification, state) => {
			let json = JSON.parse(notification.data);
	  		let alert = this.alertCtrl.create({
	      	title: json.title,
	      	subTitle: json.mydata
				});

				this.navCtrl.push(json.direccion);				
			
			this.badge.clear();
			alert.present();
		})
			
		this.localNotifications.on('action', function (notification, state, data) {
			var replyMessage;

			if (data.identifier === 'PROVIDE_INPUT') {
				replyMessage = data.responseInfoText;
				alert(replyMessage);
			} else if (data.identifier === 'SIGN_IN') {
				alert('You have been signed in!');
			} else if (data.identifier === 'MORE_SIGNIN_OPTIONS') {
				alert('(Pretend there are more signin options here, please.)');
			} 
		});
	  });
	}

	scheduleNotification() {
		/*var actions = [ {
        identifier: 'SIGN_IN',
        title: 'Yes',
        icon: 'res://ic_signin',
        activationMode: 'background',
        destructive: false,
        authenticationRequired: true
    	},{
			identifier: 'MORE_SIGNIN_OPTIONS',
			title: 'More Options',
			icon: 'res://ic_moreoptions',
			activationMode: 'foreground',
			destructive: false,
			authenticationRequired: false
		},{
			identifier: 'PROVIDE_INPUT',
			title: 'Provide Input',
			icon: 'ic_input',
			activationMode: 'background',
			destructive: false,
			authenticationRequired: false,
			behavior: 'textInput',
			textInputSendTitle: 'Reply'
		}];*/

		let momento = new Date(new Date().getTime() + 5 * 1000);
		this.plt.ready().then(()=>{
			(<any>cordova).plugins.notification.local.schedule({
			    id: this.id,
			    title: 'Attention',
			    text: this.desc,
				headsup: true,
				icon: 'res://manos',
				smallIcon: 'res://manos_small',
				sound: 'file://audio/manos.mp3',
			    data: { mydata: 'Recibiste una notificacion!', direccion: 'DestinoPage' },
				at: momento,
				//actions: [actions[0], actions[1]],
        		//category: 'SIGN_IN_TO_C-LASS'
		  	});
		})
	}

	/*setSound(){
		if (this.plt.is('android')) {
			return 'file://audio/manos.mp3'
		} else {
			return 'file://audio/manos.mp3'
		}
	}*/
	
	incrementar_badge(){
		this.badge.increase(1);
	}
}
